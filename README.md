#SAM2018

An online Conference file submission management system run in Java 8 and Spark.

## Team

- Fasaiel Alanazi

- Niharika Dalal

- Andrew DiStasi

- Raseshwari Pulle


##Prerequisites

- Java 8

- Maven


##How to run it

1. Clone the repository and go to the root directory.

2. Execute `mvn compile exec:java`

3. Open in your browser `http://localhost:4567/`

4. Start a game and begin playing.


##License
MIT License

See LICENSE for details.
